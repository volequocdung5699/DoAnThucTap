package com.example.doanthuctap.repository;

import com.example.doanthuctap.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Integer> {
    CategoryEntity findCategoryEntityById(int id);
}
